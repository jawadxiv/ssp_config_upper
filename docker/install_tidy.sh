#!/bin/bash

set -e
set -u

export DEBIAN_FRONTEND=noninteractive

apt-get update && apt-get install --no-install-recommends -yy tidy